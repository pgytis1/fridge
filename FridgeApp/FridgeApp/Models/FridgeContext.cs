﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FridgeApp.Models
{
    public partial class FridgeContext : DbContext
    {
        public virtual DbSet<Diet> Diet { get; set; }
        public virtual DbSet<DietProduct> DietProduct { get; set; }
        public virtual DbSet<FridgeUser> FridgeUser { get; set; }
        public virtual DbSet<FridgeUserProduct> FridgeUserProduct { get; set; }
        public virtual DbSet<Offer> Offer { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Recipe> Recipe { get; set; }
        public virtual DbSet<RecipeProduct> RecipeProduct { get; set; }
        public virtual DbSet<SubscribedProduct> SubscribedProduct { get; set; }

//         protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//         {
//             if (!optionsBuilder.IsConfigured)
//             {
// #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                 optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Fridge;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
//             }
//         }
        public FridgeContext(DbContextOptions<FridgeContext> options)
            : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Diet>(entity =>
            {
                entity.HasKey(e => e.IdDiet);

                entity.Property(e => e.IdDiet)
                    .HasColumnName("id_Diet")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DurationDays).HasColumnName("Duration_Days");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DietProduct>(entity =>
            {
                entity.HasKey(e => e.IdDietProduct);

                entity.Property(e => e.IdDietProduct)
                    .HasColumnName("id_DietProduct")
                    .ValueGeneratedNever();

                entity.Property(e => e.FkDietidDiet).HasColumnName("fk_Dietid_Diet");

                entity.Property(e => e.FkProductidProduct).HasColumnName("fk_Productid_Product");

                entity.Property(e => e.Units)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.FkDietidDietNavigation)
                    .WithMany(p => p.DietProduct)
                    .HasForeignKey(d => d.FkDietidDiet)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("may_have6");

                entity.HasOne(d => d.FkProductidProductNavigation)
                    .WithMany(p => p.DietProduct)
                    .HasForeignKey(d => d.FkProductidProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("may_have4");
            });

            modelBuilder.Entity<FridgeUser>(entity =>
            {
                entity.HasKey(e => e.IdFridgeUser);

                entity.Property(e => e.IdFridgeUser)
                    .HasColumnName("id_FridgeUser")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FridgeUserProduct>(entity =>
            {
                entity.HasKey(e => e.IdFridgeUserProduct);

                entity.Property(e => e.IdFridgeUserProduct)
                    .HasColumnName("id_FridgeUserProduct")
                    .ValueGeneratedNever();

                entity.Property(e => e.FkFridgeUseridFridgeUser).HasColumnName("fk_FridgeUserid_FridgeUser");

                entity.Property(e => e.FkProductidProduct).HasColumnName("fk_Productid_Product");

                entity.Property(e => e.Units)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.FkFridgeUseridFridgeUserNavigation)
                    .WithMany(p => p.FridgeUserProduct)
                    .HasForeignKey(d => d.FkFridgeUseridFridgeUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("may_have");

                entity.HasOne(d => d.FkProductidProductNavigation)
                    .WithMany(p => p.FridgeUserProduct)
                    .HasForeignKey(d => d.FkProductidProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("may_have3");
            });

            modelBuilder.Entity<Offer>(entity =>
            {
                entity.HasKey(e => e.IdOffer);

                entity.Property(e => e.IdOffer)
                    .HasColumnName("id_Offer")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DiscountedPrice).HasColumnName("Discounted_price");

                entity.Property(e => e.EndDate)
                    .HasColumnName("End_date")
                    .HasColumnType("date");

                entity.Property(e => e.FkProductidProduct).HasColumnName("fk_Productid_Product");

                entity.Property(e => e.StartDate)
                    .HasColumnName("Start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UssualPrice).HasColumnName("Ussual_price");

                entity.HasOne(d => d.FkProductidProductNavigation)
                    .WithMany(p => p.Offer)
                    .HasForeignKey(d => d.FkProductidProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("may_have5");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.IdProduct);

                entity.Property(e => e.IdProduct)
                    .HasColumnName("id_Product")
                    .ValueGeneratedNever();

                entity.Property(e => e.FkDietidDiet).HasColumnName("fk_Dietid_Diet");

                entity.Property(e => e.FkRecipeidRecipe).HasColumnName("fk_Recipeid_Recipe");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QuantityType)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.FkDietidDietNavigation)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.FkDietidDiet)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Product__fk_Diet__29572725");

                entity.HasOne(d => d.FkRecipeidRecipeNavigation)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.FkRecipeidRecipe)
                    .HasConstraintName("FK__Product__fk_Reci__2A4B4B5E");
            });

            modelBuilder.Entity<Recipe>(entity =>
            {
                entity.HasKey(e => e.IdRecipe);

                entity.Property(e => e.IdRecipe)
                    .HasColumnName("id_Recipe")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RecipeProduct>(entity =>
            {
                entity.HasKey(e => e.IdRecipeProduct);

                entity.Property(e => e.IdRecipeProduct)
                    .HasColumnName("id_RecipeProduct")
                    .ValueGeneratedNever();

                entity.Property(e => e.FkProductidProduct).HasColumnName("fk_Productid_Product");

                entity.Property(e => e.FkRecipeidRecipe).HasColumnName("fk_Recipeid_Recipe");

                entity.Property(e => e.Units)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SubscribedProduct>(entity =>
            {
                entity.HasKey(e => e.IdSubscribedProduct);

                entity.Property(e => e.IdSubscribedProduct)
                    .HasColumnName("id_SubscribedProduct")
                    .ValueGeneratedNever();

                entity.Property(e => e.FkFridgeUseridFridgeUser).HasColumnName("fk_FridgeUserid_FridgeUser");

                entity.Property(e => e.FkProductidProduct).HasColumnName("fk_Productid_Product");

                entity.HasOne(d => d.FkFridgeUseridFridgeUserNavigation)
                    .WithMany(p => p.SubscribedProduct)
                    .HasForeignKey(d => d.FkFridgeUseridFridgeUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Subscribe__fk_Fr__35BCFE0A");

                entity.HasOne(d => d.FkProductidProductNavigation)
                    .WithMany(p => p.SubscribedProduct)
                    .HasForeignKey(d => d.FkProductidProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Subscribe__fk_Pr__34C8D9D1");
            });
        }
    }
}
