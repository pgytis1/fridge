﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FridgeApp.Models
{
    public partial class FridgeUserProduct
    {
        public double? Quantity { get; set; }
        public string Units { get; set; }
        public double? Threshold { get; set; }
        public int IdFridgeUserProduct { get; set; }
        public int FkFridgeUseridFridgeUser { get; set; }
        public int FkProductidProduct { get; set; }
        //public Recipe FkRecipeidRecipeNavigation { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public FridgeUser FkFridgeUseridFridgeUserNavigation { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public Product FkProductidProductNavigation { get; set; }
    }
}
