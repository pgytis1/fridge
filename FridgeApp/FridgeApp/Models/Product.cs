﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FridgeApp.Models
{
    public partial class Product
    {
        public Product()
        {
            DietProduct = new HashSet<DietProduct>();
            FridgeUserProduct = new HashSet<FridgeUserProduct>();
            Offer = new HashSet<Offer>();
            SubscribedProduct = new HashSet<SubscribedProduct>();
        }

        public double? Quantity { get; set; }
        public double? Threshold { get; set; }
        public string QuantityType { get; set; }
        public string Name { get; set; }
        public double? Carbs { get; set; }
        public double? Proteins { get; set; }
        public double? Calories { get; set; }
        public double? Fats { get; set; }
        public int IdProduct { get; set; }
        public int FkDietidDiet { get; set; }
        public int? FkRecipeidRecipe { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public Diet FkDietidDietNavigation { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public Recipe FkRecipeidRecipeNavigation { get; set; }
        public ICollection<DietProduct> DietProduct { get; set; }
        public ICollection<FridgeUserProduct> FridgeUserProduct { get; set; }
        public ICollection<Offer> Offer { get; set; }
        public ICollection<SubscribedProduct> SubscribedProduct { get; set; }
    }
}
