﻿using System;
using System.Collections.Generic;

namespace FridgeApp.Models
{
    public partial class Recipe
    {
        public Recipe()
        {
            Product = new HashSet<Product>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public int IdRecipe { get; set; }

        public ICollection<Product> Product { get; set; }
    }
}
