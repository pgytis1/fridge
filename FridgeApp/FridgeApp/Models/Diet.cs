﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FridgeApp.Models
{
    public partial class Diet
    {
        public Diet()
        {
            DietProduct = new HashSet<DietProduct>();
            Product = new HashSet<Product>();
        }

        public string Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? DurationDays { get; set; }
        public int IdDiet { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public ICollection<DietProduct> DietProduct { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public ICollection<Product> Product { get; set; }
    }
}
