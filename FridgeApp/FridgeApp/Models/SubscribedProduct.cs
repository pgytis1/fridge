﻿using System;
using System.Collections.Generic;

namespace FridgeApp.Models
{
    public partial class SubscribedProduct
    {
        public int IdSubscribedProduct { get; set; }
        public int FkProductidProduct { get; set; }
        public int FkFridgeUseridFridgeUser { get; set; }

        public FridgeUser FkFridgeUseridFridgeUserNavigation { get; set; }
        public Product FkProductidProductNavigation { get; set; }
    }
}
