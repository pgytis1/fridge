﻿using System;
using System.Collections.Generic;

namespace FridgeApp.Models
{
    public partial class RecipeProduct
    {
        public double? Quantity { get; set; }
        public string Units { get; set; }
        public int IdRecipeProduct { get; set; }
        public int FkRecipeidRecipe { get; set; }
        public int FkProductidProduct { get; set; }
    }
}
