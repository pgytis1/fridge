﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FridgeApp.Models
{
    public partial class DietProduct
    {
        public double? Quantity { get; set; }
        public string Units { get; set; }
        public int IdDietProduct { get; set; }
        public int FkDietidDiet { get; set; }
        public int FkProductidProduct { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public Diet FkDietidDietNavigation { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public Product FkProductidProductNavigation { get; set; }
    }
}
