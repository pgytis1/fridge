﻿using System;
using System.Collections.Generic;

namespace FridgeApp.Models
{
    public partial class Offer
    {
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? Discount { get; set; }
        public double? UssualPrice { get; set; }
        public double? DiscountedPrice { get; set; }
        public int IdOffer { get; set; }
        public int FkProductidProduct { get; set; }

        public Product FkProductidProductNavigation { get; set; }
    }
}
