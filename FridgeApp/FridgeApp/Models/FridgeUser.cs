﻿using System;
using System.Collections.Generic;

namespace FridgeApp.Models
{
    public partial class FridgeUser
    {
        public FridgeUser()
        {
            FridgeUserProduct = new HashSet<FridgeUserProduct>();
            SubscribedProduct = new HashSet<SubscribedProduct>();
        }

        public bool? IsAdmin { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Surname { get; set; }
        public int IdFridgeUser { get; set; }

        public ICollection<FridgeUserProduct> FridgeUserProduct { get; set; }
        public ICollection<SubscribedProduct> SubscribedProduct { get; set; }
    }
}
