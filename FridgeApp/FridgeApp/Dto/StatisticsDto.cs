﻿using FridgeApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FridgeApp.Dto
{
    public class StatisticsDto
    {
        public ProductStatsDto FreshProduct { get; set; }
        public ProductStatsDto OldProduct { get; set; }
        public ProductStatsDto CalorificProduct { get; set; }
    }
        public class ProductStatsDto
        {
            public String Name { get; set; }
            public double Carbs { get; set; }
            public double Calories { get; set; }
            public double Fats { get; set; }
            public double Proteins { get; set; }
        }
}
