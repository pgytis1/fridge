﻿using FridgeApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FridgeApp.Dto
{
    public class GenRecipeDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int IdRecipe { get; set; }

        public List<RecipeDto> Products { get; set; }
    }
    public class RecipeDto
    {
        public int id { get; set; }
        public string Name { get; set; }
        public double? Quantity { get; set; }
        public string Units { get; set; }
    }
}
