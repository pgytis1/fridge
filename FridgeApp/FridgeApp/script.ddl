--@(#) script.ddl

CREATE TABLE Recipe
(
	Name varchar (255),
	Description varchar (255),
	id_Recipe integer,
	PRIMARY KEY(id_Recipe)
);

CREATE TABLE Diet
(
	Type varchar (255),
	Name varchar (255),
	Description varchar (255),
	Duration_Days integer,
	id_Diet integer,
	PRIMARY KEY(id_Diet)
);

CREATE TABLE User
(
	IsAdmin boolean,
	Name varchar (255),
	Password varchar (255),
	Surname varchar (255),
	id_User integer,
	PRIMARY KEY(id_User)
);

CREATE TABLE Product
(
	Quantity double precision,
	Threshold double precision,
	QuantityType varchar (255),
	Name varchar (255),
	Carbs double precision,
	Proteins double precision,
	Calories double precision,
	Fats double precision,
	id_Product integer,
	fk_Dietid_Diet integer NOT NULL,
	fk_Recipeid_Recipe integer,
	PRIMARY KEY(id_Product),
	FOREIGN KEY(fk_Dietid_Diet) REFERENCES Diet (id_Diet),
	FOREIGN KEY(fk_Recipeid_Recipe) REFERENCES Recipe (id_Recipe)
);

CREATE TABLE DietProduct
(
	Quantity double precision,
	Units varchar (255),
	id_DietProduct integer,
	fk_Dietid_Diet integer NOT NULL,
	fk_Productid_Product integer NOT NULL,
	PRIMARY KEY(id_DietProduct),
	CONSTRAINT may_have6 FOREIGN KEY(fk_Dietid_Diet) REFERENCES Diet (id_Diet),
	CONSTRAINT may_have4 FOREIGN KEY(fk_Productid_Product) REFERENCES Product (id_Product)
);

CREATE TABLE UserProduct
(
	Quantity double precision,
	Units varchar (255),
	Threshold double precision,
	id_UserProduct integer,
	fk_Userid_User integer NOT NULL,
	fk_Productid_Product integer NOT NULL,
	PRIMARY KEY(id_UserProduct),
	CONSTRAINT may_have FOREIGN KEY(fk_Userid_User) REFERENCES User (id_User),
	CONSTRAINT may_have3 FOREIGN KEY(fk_Productid_Product) REFERENCES Product (id_Product)
);

CREATE TABLE SubscribedProduct
(
	id_SubscribedProduct integer,
	fk_Productid_Product integer NOT NULL,
	fk_Userid_User integer NOT NULL,
	PRIMARY KEY(id_SubscribedProduct),
	FOREIGN KEY(fk_Productid_Product) REFERENCES Product (id_Product),
	FOREIGN KEY(fk_Userid_User) REFERENCES User (id_User)
);

CREATE TABLE RecipeProduct
(
	Quantity double precision,
	Units varchar (255),
	id_RecipeProduct integer,
	fk_Recipeid_Recipe integer NOT NULL,
	fk_Productid_Product integer NOT NULL,
	PRIMARY KEY(id_RecipeProduct),
	CONSTRAINT consists of FOREIGN KEY(fk_Recipeid_Recipe) REFERENCES Recipe (id_Recipe),
	CONSTRAINT may_have2 FOREIGN KEY(fk_Productid_Product) REFERENCES Product (id_Product)
);

CREATE TABLE Offer
(
	Description varchar (255),
	Start_date date,
	End_date date,
	Discount double precision,
	Ussual_price double precision,
	Discounted_price double precision,
	id_Offer integer,
	fk_Productid_Product integer NOT NULL,
	PRIMARY KEY(id_Offer),
	CONSTRAINT may_have5 FOREIGN KEY(fk_Productid_Product) REFERENCES Product (id_Product)
);
