﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FridgeApp.Models;
using MoreLinq;
using FridgeApp.Dto;
using Newtonsoft.Json;

namespace FridgeApp.Controllers
{
    [Produces("application/json")]
    //[Route("api/recipe")]
    public class RecipeController : Controller
    {
        private readonly FridgeContext _context;

        public RecipeController(FridgeContext context)
        {
            _context = context;
        }
        [HttpGet]
        [Route("api/GetAllNeedProducts")]
        public async Task<IActionResult> GetAllNeedProducts()
        {
            var User = await _context.FridgeUser.SingleOrDefaultAsync(user => user.IdFridgeUser == 2);
            var GetAllNeedProducts = _context.FridgeUserProduct
              .Where(p => p.FkFridgeUseridFridgeUser == User.IdFridgeUser)
              .Include(p => p.FkProductidProductNavigation)
              .Where(p => p.Threshold < p.Quantity)
              .Select(p => new
              {
                  id = p.FkProductidProductNavigation.IdProduct,
                  name = p.FkProductidProductNavigation.Name,
                  quantity = p.Quantity,
              }).ToList();
            //Console.WriteLine("oksdf sd ");
            // var RrsultDto = GetAllNeedProducts
            GetAllNeedProducts.INFO();
            return Ok(GetAllNeedProducts);

        }
        [HttpGet]
        [Route("api/CreateRecipe")]
        public async Task<IActionResult> CreateRecipe()
        {
            var User = await _context.FridgeUser.SingleOrDefaultAsync(user => user.IdFridgeUser == 2);


            var GetFridgeProducts = _context.FridgeUserProduct
              .Where(p => p.FkFridgeUseridFridgeUser == User.IdFridgeUser)
              .Include(p => p.FkProductidProductNavigation);


            var GetProducts = GetFridgeProducts
                .Include(p => p.FkProductidProductNavigation)
                .Select(p => p);

            GetProducts.INFO();
            var MostMatchedRecipeID = _context.RecipeProduct.GroupBy(r => r.FkRecipeidRecipe)
                .Select(r =>
                new
                {
                    RecipeID = r.First().FkRecipeidRecipe,
                    Products = r.ToList(),
                    productsCount = r.Select(k => k.IdRecipeProduct).Count(id => GetProducts.Select(p => p.FkProductidProduct).Contains(id))
                }).MaxBy(r => r.productsCount);

            var Recipe = _context.Recipe
                .SingleOrDefault(r => r.IdRecipe == MostMatchedRecipeID.RecipeID);

            var RecipeDto = new GenRecipeDto
            {
                Description = Recipe.Description,
                IdRecipe = Recipe.IdRecipe,
                Name = Recipe.Name,
                Products = MostMatchedRecipeID.Products.Select(p => new RecipeDto
                {
                    id = p.FkProductidProduct,
                    Name = _context.Product.SingleOrDefault(prod => prod.IdProduct == p.FkProductidProduct).Name,
                    Quantity = p.Quantity,
                    Units = p.Units
                }).ToList()
            };
            return Ok(RecipeDto);

        }
    }
}


public static class MyExtensions
{
    public static void INFO(this object obj)
    {
        var str = JsonConvert.SerializeObject(obj, Formatting.Indented);
        Console.WriteLine($"------------------{nameof(obj)}--------------------");
        Console.WriteLine(str);
    }
}