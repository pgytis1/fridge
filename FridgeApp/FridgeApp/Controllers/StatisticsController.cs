﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FridgeApp.Models;
using MoreLinq;
using FridgeApp.Dto;

namespace FridgeApp.Controllers
{
    [Produces("application/json")]
    [Route("api/statistics")]
    public class StatisticsController : Controller
    {
        private readonly FridgeContext _context;

        public StatisticsController(FridgeContext context)
        {
            _context = context;
        }

        // GET: api/GetStatistics
        [HttpGet]
        public async Task<IActionResult> GetStatistics()
        {
            var fresh = await SelectFresh();
            var old = await SelectOld();
            var calorific = await SelectCalorific();
            //await Task.WhenAll(fresh, old, calorific);

            Product[] tasks = { fresh.FkProductidProductNavigation, old.FkProductidProductNavigation, calorific.FkProductidProductNavigation };
            // Product[] tasks = {fresh.Result.FkProductidProductNavigation, old.Result.FkProductidProductNavigation, calorific.Result.FkProductidProductNavigation};
            var data = new StatisticsDto
            {
                FreshProduct = new ProductStatsDto
                {
                    Name = tasks[0].Name,
                    Calories = tasks[0].Calories.Value,
                    Carbs = tasks[0].Carbs.Value,
                    Fats = tasks[0].Fats.Value,
                    Proteins = tasks[0].Proteins.Value,

                },
                OldProduct = new ProductStatsDto
                {
                    Name = tasks[1].Name,
                    Calories = tasks[1].Calories.Value,
                    Carbs = tasks[1].Carbs.Value,
                    Fats = tasks[1].Fats.Value,
                    Proteins = tasks[1].Proteins.Value,

                },
                CalorificProduct = new ProductStatsDto
                {
                    Name = tasks[2].Name,
                    Calories = tasks[2].Calories.Value,
                    Carbs = tasks[2].Carbs.Value,
                    Fats = tasks[2].Fats.Value,
                    Proteins = tasks[2].Proteins.Value,
                },
            };
            return Ok(data);
        }
        private Task<FridgeUserProduct> SelectFresh()
        {
            return _context.FridgeUserProduct.LastOrDefaultAsync();
        }
        private Task<FridgeUserProduct> SelectOld()
        {
            return _context.FridgeUserProduct.FirstAsync();
        }
        private Task<FridgeUserProduct> SelectCalorific()
        {
            return Task.Run(() =>
            {
                return _context.FridgeUser
            .Include(p => p.FridgeUserProduct)
            .Single(user => user.IdFridgeUser == 2)
            .FridgeUserProduct
            .MaxBy(p =>
            {
                var product = _context.Product.SingleOrDefault(prod => prod.IdProduct == p.FkProductidProduct);
                return p.Quantity * (product.Calories / 100);
            });
            }
                  );
        }
    }
}