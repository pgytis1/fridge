﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FridgeApp.Models;

namespace FridgeApp.Controllers
{
    [Produces("application/json")]
    [Route("api/FridgeUserProducts")]
    public class FridgeUserProductsController : Controller
    {
        private readonly FridgeContext _context;

        public FridgeUserProductsController(FridgeContext context)
        {
            _context = context;
        }

        // GET: api/FridgeUserProducts
        [HttpGet]
        public IEnumerable<FridgeUserProduct> GetFridgeUserProduct()
        {
            return _context.FridgeUserProduct;
        }

        // GET: api/FridgeUserProducts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFridgeUserProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fridgeUserProduct = await _context.FridgeUserProduct.SingleOrDefaultAsync(m => m.IdFridgeUserProduct == id);

            if (fridgeUserProduct == null)
            {
                return NotFound();
            }

            return Ok(fridgeUserProduct);
        }

        // PUT: api/FridgeUserProducts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFridgeUserProduct([FromRoute] int id, [FromBody] FridgeUserProduct fridgeUserProduct)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fridgeUserProduct.IdFridgeUserProduct)
            {
                return BadRequest();
            }

            _context.Entry(fridgeUserProduct).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FridgeUserProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FridgeUserProducts
        [HttpPost]
        public async Task<IActionResult> PostFridgeUserProduct([FromBody] FridgeUserProduct fridgeUserProduct)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FridgeUserProduct.Add(fridgeUserProduct);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FridgeUserProductExists(fridgeUserProduct.IdFridgeUserProduct))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFridgeUserProduct", new { id = fridgeUserProduct.IdFridgeUserProduct }, fridgeUserProduct);
        }

        // DELETE: api/FridgeUserProducts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFridgeUserProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fridgeUserProduct = await _context.FridgeUserProduct.SingleOrDefaultAsync(m => m.IdFridgeUserProduct == id);
            if (fridgeUserProduct == null)
            {
                return NotFound();
            }

            _context.FridgeUserProduct.Remove(fridgeUserProduct);
            await _context.SaveChangesAsync();

            return Ok(fridgeUserProduct);
        }

        private bool FridgeUserProductExists(int id)
        {
            return _context.FridgeUserProduct.Any(e => e.IdFridgeUserProduct == id);
        }
    }
}