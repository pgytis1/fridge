USE [Fridge]
GO
/****** Object:  Table [dbo].[Diet]    Script Date: 2018-05-13 17:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Diet](
	[Type] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[Duration_Days] [int] NULL,
	[id_Diet] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Diet] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DietProduct]    Script Date: 2018-05-13 17:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DietProduct](
	[Quantity] [float] NULL,
	[Units] [varchar](255) NULL,
	[id_DietProduct] [int] NOT NULL,
	[fk_Dietid_Diet] [int] NOT NULL,
	[fk_Productid_Product] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_DietProduct] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FridgeUser]    Script Date: 2018-05-13 17:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FridgeUser](
	[IsAdmin] [bit] NULL,
	[Name] [varchar](255) NULL,
	[Password] [varchar](255) NULL,
	[Surname] [varchar](255) NULL,
	[id_FridgeUser] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_FridgeUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FridgeUserProduct]    Script Date: 2018-05-13 17:41:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FridgeUserProduct](
	[Quantity] [float] NULL,
	[Units] [varchar](255) NULL,
	[Threshold] [float] NULL,
	[id_FridgeUserProduct] [int] NOT NULL,
	[fk_FridgeUserid_FridgeUser] [int] NOT NULL,
	[fk_Productid_Product] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_FridgeUserProduct] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Offer]    Script Date: 2018-05-13 17:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Offer](
	[Description] [varchar](255) NULL,
	[Start_date] [date] NULL,
	[End_date] [date] NULL,
	[Discount] [float] NULL,
	[Ussual_price] [float] NULL,
	[Discounted_price] [float] NULL,
	[id_Offer] [int] NOT NULL,
	[fk_Productid_Product] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Offer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 2018-05-13 17:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Quantity] [float] NULL,
	[Threshold] [float] NULL,
	[QuantityType] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[Carbs] [float] NULL,
	[Proteins] [float] NULL,
	[Calories] [float] NULL,
	[Fats] [float] NULL,
	[id_Product] [int] NOT NULL,
	[fk_Dietid_Diet] [int] NOT NULL,
	[fk_Recipeid_Recipe] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Product] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recipe]    Script Date: 2018-05-13 17:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recipe](
	[Name] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[id_Recipe] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Recipe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecipeProduct]    Script Date: 2018-05-13 17:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecipeProduct](
	[Quantity] [float] NULL,
	[Units] [varchar](255) NULL,
	[id_RecipeProduct] [int] NOT NULL,
	[fk_Recipeid_Recipe] [int] NOT NULL,
	[fk_Productid_Product] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_RecipeProduct] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubscribedProduct]    Script Date: 2018-05-13 17:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscribedProduct](
	[id_SubscribedProduct] [int] NOT NULL,
	[fk_Productid_Product] [int] NOT NULL,
	[fk_FridgeUserid_FridgeUser] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_SubscribedProduct] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'viverra', N'hac', N'gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer', 58, 1)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'iaculis', N'nullam', N'in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur', 74, 2)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'sagittis', N'justo', N'id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo', 60, 3)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'eros', N'potenti', N'pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut', 67, 4)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'dictumst', N'ut', N'purus eu magna vulputate luctus cum sociis natoque penatibus et', 52, 5)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'etiam', N'metus', N'aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer', 35, 6)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'elit', N'ultrices', N'amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci', 33, 7)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'aenean', N'posuere', N'purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in', 87, 8)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'condimentum', N'cursus', N'id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae', 96, 9)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'integer', N'dolor', N'porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio', 12, 10)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'turpis', N'consectetuer', N'a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit', 97, 11)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'morbi', N'sit', N'felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar', 27, 12)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'justo', N'ante', N'id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci', 41, 13)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'viverra', N'vestibulum', N'justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est', 7, 14)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'leo', N'morbi', N'sit amet consectetuer adipiscing elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante', 58, 15)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'quis', N'metus', N'urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam', 76, 16)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'pellentesque', N'velit', N'phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate', 89, 17)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'ullamcorper', N'pretium', N'sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti', 32, 18)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'non', N'erat', N'orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum', 33, 19)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'lectus', N'luctus', N'est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl', 2, 20)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'erat', N'metus', N'rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit', 12, 21)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'tristique', N'lorem', N'nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu', 26, 22)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'magna', N'augue', N'dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit', 19, 23)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'consequat', N'odio', N'cras pellentesque volutpat dui maecenas tristique est et tempus semper', 80, 24)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'nulla', N'nam', N'parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque', 40, 25)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'ornare', N'augue', N'pede lobortis ligula sit amet eleifend pede libero quis orci nullam', 96, 26)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'vivamus', N'dui', N'quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet', 8, 27)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'mi', N'luctus', N'justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus', 97, 28)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'dictumst', N'ipsum', N'imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat', 72, 29)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'ipsum', N'libero', N'in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id', 63, 30)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'nisl', N'lacinia', N'lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer', 14, 31)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'ullamcorper', N'nam', N'pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut', 40, 32)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'turpis', N'mauris', N'rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id', 29, 33)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'nullam', N'diam', N'ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue', 38, 34)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'venenatis', N'tortor', N'vel nulla eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla', 47, 35)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'blandit', N'sit', N'proin eu mi nulla ac enim in tempor turpis nec euismod', 68, 36)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'euismod', N'hac', N'molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus', 94, 37)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'non', N'sed', N'in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices', 41, 38)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'rhoncus', N'a', N'vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero', 46, 39)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'in', N'vestibulum', N'hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum', 56, 40)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'tempus', N'lacinia', N'dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus', 3, 41)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'nunc', N'pede', N'sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis', 26, 42)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'in', N'mi', N'faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse', 88, 43)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'turpis', N'felis', N'enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum', 49, 44)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'cubilia', N'velit', N'vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat', 54, 45)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'id', N'odio', N'quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non', 68, 46)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'sed', N'in', N'phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at', 99, 47)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'massa', N'et', N'leo odio porttitor id consequat in consequat ut nulla sed', 6, 48)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'tincidunt', N'volutpat', N'id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque', 53, 49)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'vel', N'vulputate', N'nam nulla integer pede justo lacinia eget tincidunt eget tempus', 8, 50)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'mattis', N'maecenas', N'faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat', 6, 51)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'ullamcorper', N'dui', N'tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam', 100, 52)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'justo', N'id', N'pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet', 66, 53)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'aliquam', N'luctus', N'iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut', 90, 54)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'interdum', N'odio', N'natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor', 31, 55)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'turpis', N'non', N'amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed', 3, 56)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'proin', N'sapien', N'imperdiet nullam orci pede venenatis non sodales sed tincidunt eu', 95, 57)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'aliquam', N'fermentum', N'turpis integer aliquet massa id lobortis convallis tortor risus dapibus', 57, 58)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'ligula', N'aliquam', N'sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl', 64, 59)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'curabitur', N'risus', N'augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla', 3, 60)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'neque', N'vivamus', N'aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper', 21, 61)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'dictumst', N'leo', N'mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis', 27, 62)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'eu', N'pellentesque', N'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse', 72, 63)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'dapibus', N'malesuada', N'sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui', 11, 64)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'orci', N'eu', N'nisl aenean lectus pellentesque eget nunc donec quis orci eget orci', 33, 65)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'integer', N'a', N'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor', 21, 66)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'vestibulum', N'platea', N'felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed', 97, 67)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'parturient', N'pede', N'leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices', 79, 68)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'in', N'suscipit', N'magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque', 7, 69)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'vestibulum', N'ut', N'lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet', 38, 70)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'commodo', N'vestibulum', N'justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing', 66, 71)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'rutrum', N'ipsum', N'amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus', 10, 72)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'sagittis', N'non', N'neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices', 70, 73)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'interdum', N'velit', N'quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel', 57, 74)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'turpis', N'vulputate', N'morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac', 45, 75)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'est', N'erat', N'non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie', 74, 76)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'lacus', N'pulvinar', N'sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut', 86, 77)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'proin', N'nam', N'enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis', 69, 78)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'cursus', N'integer', N'molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat', 40, 79)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'posuere', N'blandit', N'cras pellentesque volutpat dui maecenas tristique est et tempus semper', 25, 80)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'est', N'tincidunt', N'platea dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat', 99, 81)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'dapibus', N'amet', N'congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam', 52, 82)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'nisl', N'at', N'dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus', 62, 83)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'blandit', N'adipiscing', N'consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel', 2, 84)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'ultrices', N'amet', N'congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat', 18, 85)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'dictumst', N'varius', N'proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum', 87, 86)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'tincidunt', N'varius', N'vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis', 73, 87)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'purus', N'sed', N'viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper interdum mauris', 57, 88)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'habitasse', N'sit', N'interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla', 80, 89)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'ligula', N'convallis', N'penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean', 84, 90)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'convallis', N'mus', N'auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit', 36, 91)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'sit', N'massa', N'ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam', 47, 92)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'morbi', N'rutrum', N'leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede', 64, 93)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'nulla', N'turpis', N'feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare', 64, 94)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'eleifend', N'parturient', N'ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras', 17, 95)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'aliquet', N'aliquam', N'pellentesque ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate', 49, 96)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'curabitur', N'quis', N'non sodales sed tincidunt eu felis fusce posuere felis sed lacus', 4, 97)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'tincidunt', N'nulla', N'varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus', 10, 98)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'vel', N'nunc', N'orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum', 54, 99)
INSERT [dbo].[Diet] ([Type], [Name], [Description], [Duration_Days], [id_Diet]) VALUES (N'elementum', N'erat', N'odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus', 66, 100)
GO
INSERT [dbo].[FridgeUser] ([IsAdmin], [Name], [Password], [Surname], [id_FridgeUser]) VALUES (1, N'AdminName', N'Kaunas', N'Admin', 1)
INSERT [dbo].[FridgeUser] ([IsAdmin], [Name], [Password], [Surname], [id_FridgeUser]) VALUES (0, N'Martynas', N'Kaunas', N'Pocius', 2)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (537.82, N'grams', 24.14, 21, 2, 66)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (590.33, N'grams', 78.15, 40, 1, 57)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (671.06, N'grams', 75.16, 50, 1, 31)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (615.65, N'grams', 84.33, 73, 1, 22)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (32.92, N'grams', 3.83, 122, 1, 82)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (754.11, N'grams', 17.19, 132, 2, 7)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (147.22, N'grams', 34.94, 211, 2, 11)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (303.16, N'grams', 17.37, 259, 2, 94)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (12.32, N'grams', 64.17, 279, 2, 30)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (487.61, N'grams', 90.12, 284, 2, 78)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (935.98, N'grams', 6.66, 343, 1, 95)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (330.12, N'grams', 83.12, 347, 2, 100)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (993.04, N'grams', 40.04, 450, 2, 75)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (817.91, N'grams', 42.35, 454, 1, 9)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (319.69, N'grams', 94.77, 473, 1, 41)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (631.7, N'grams', 78.03, 523, 1, 78)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (410.58, N'grams', 25.25, 547, 2, 85)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (296.96, N'grams', 73.17, 597, 2, 99)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (465, N'grams', 41.59, 632, 2, 81)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (717.48, N'grams', 11.65, 653, 1, 25)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (886.22, N'grams', 10.64, 686, 1, 11)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (477.19, N'grams', 35.51, 726, 1, 21)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (578.96, N'grams', 74.97, 748, 1, 66)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (510.98, N'grams', 42.62, 823, 1, 51)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (109.75, N'grams', 95.03, 898, 1, 3)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (717.99, N'grams', 32.22, 913, 1, 68)
INSERT [dbo].[FridgeUserProduct] ([Quantity], [Units], [Threshold], [id_FridgeUserProduct], [fk_FridgeUserid_FridgeUser], [fk_Productid_Product]) VALUES (820.8, N'grams', 32.07, 927, 2, 53)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (5.35, 44.63, N'grams', N'Sobe - Cranberry Grapefruit', 27.98, 15.49, 39.01, 30.9, 1, 32, 8)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (92.95, 86.96, N'grams', N'Halibut - Whole, Fresh', 70.06, 45.63, 80.96, 63.73, 2, 8, 85)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (72.75, 66.57, N'grams', N'Pepper - Red Thai', 72.28, 77.07, 65.57, 82.69, 3, 57, 38)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (62.01, 53.09, N'grams', N'Savory', 48.82, 28.86, 60.15, 98.94, 4, 20, 29)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (29.7, 43.52, N'grams', N'Pomegranates', 52.72, 73.69, 62.64, 78.76, 5, 35, 8)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (22.42, 39.69, N'grams', N'Garam Marsala', 45.93, 85.38, 83.42, 21.71, 6, 7, 44)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (32.35, 90.32, N'grams', N'Sausage - Liver', 87.25, 6.57, 84.27, 64.91, 7, 47, 40)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (74.91, 90.47, N'grams', N'Onion Powder', 91.64, 78.7, 92.89, 89.79, 8, 62, 66)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (19.47, 58.74, N'grams', N'Muskox - French Rack', 58.21, 91.02, 67.79, 53.51, 9, 33, 51)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (63.32, 91.39, N'grams', N'Wine - Red, Harrow Estates, Cab', 53.86, 92.44, 89.79, 14.6, 10, 58, 83)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (28.62, 66.35, N'grams', N'Red Currants', 29.85, 85.51, 31.94, 5.2, 11, 77, 25)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (78.53, 33.88, N'grams', N'Wine - Merlot Vina Carmen', 5.96, 17.53, 41.34, 3.75, 12, 80, 52)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (23.66, 91.82, N'grams', N'Filo Dough', 9.68, 87.76, 37.57, 52.77, 13, 28, 32)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (49.04, 62.82, N'grams', N'Onions - Cippolini', 53.04, 63.3, 3.56, 76.48, 14, 24, 44)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (21.41, 52.36, N'grams', N'Napkin White - Starched', 92.72, 44.29, 99.01, 53.38, 15, 6, 73)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (2.14, 72.85, N'grams', N'Wine - Ruffino Chianti Classico', 57.14, 5.14, 89.83, 97.26, 16, 52, 32)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (65.1, 23.07, N'grams', N'Cheese - La Sauvagine', 20.46, 37.88, 51.59, 85.68, 17, 34, 19)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (40.11, 88.66, N'grams', N'Melon - Cantaloupe', 19.37, 95.93, 58.05, 95.05, 18, 85, 48)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (60.9, 11.84, N'grams', N'Juice Peach Nectar', 95.19, 24.85, 14.58, 62.27, 19, 70, 72)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (49.19, 45.78, N'grams', N'Flounder - Fresh', 82.73, 19.81, 99.37, 55.63, 20, 42, 32)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (40.51, 25.97, N'grams', N'Nantucket - Orange Mango Cktl', 94.04, 79.96, 58.31, 33.96, 21, 75, 68)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (15.59, 30.55, N'grams', N'Tomatoes - Yellow Hot House', 58.66, 35.8, 61.96, 4.38, 22, 81, 67)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (44.85, 15.54, N'grams', N'Gatorade - Orange', 52.78, 4.89, 91.86, 54.54, 23, 33, 27)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (15.88, 50.04, N'grams', N'Lid - 10,12,16 Oz', 33.64, 11.32, 68.23, 35.05, 24, 56, 28)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (43.79, 73.05, N'grams', N'Cheese - Blue', 8.7, 5, 99.05, 68.89, 25, 63, 38)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (25.69, 29.8, N'grams', N'Pasta - Penne, Lisce, Dry', 69.21, 50, 50.17, 90.39, 26, 14, 42)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (84.39, 35.56, N'grams', N'Creme De Banane - Marie', 52.78, 48.74, 86.74, 37.5, 27, 22, 44)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (86.06, 40.98, N'grams', N'Dried Cherries', 12.23, 41.53, 68.26, 12.69, 28, 71, 83)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (72.03, 49.75, N'grams', N'Compound - Rum', 45.97, 57.66, 95.48, 59.82, 29, 41, 67)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (22.68, 55.13, N'grams', N'Praline Paste', 81.18, 64.9, 86.35, 64.92, 30, 58, 57)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (46.22, 49.47, N'grams', N'Cabbage - Savoy', 6.54, 54.91, 24.68, 54.55, 31, 84, 41)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (33.2, 49.39, N'grams', N'Persimmons', 98.34, 36.37, 51.22, 96.08, 32, 98, 29)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (31.97, 99.93, N'grams', N'Glaze - Apricot', 99.4, 22.71, 9.03, 48.51, 33, 11, 12)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (52.01, 66.76, N'grams', N'Pepper - Scotch Bonnet', 86.04, 54.38, 52.91, 74.34, 34, 55, 4)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (39.38, 2.6, N'grams', N'Beef - Tenderloin', 92.85, 71.73, 93.28, 43.46, 35, 91, 57)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (50.13, 39.43, N'grams', N'Wine - Vouvray Cuvee Domaine', 71.35, 17.75, 73.47, 98.11, 36, 47, 19)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (34.27, 39.77, N'grams', N'Dried Apple', 31.48, 56.4, 66.58, 62.94, 37, 21, 79)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (75.54, 34.14, N'grams', N'Cookie - Dough Variety', 12.76, 39.79, 87.49, 91.38, 38, 18, 24)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (95.79, 57.8, N'grams', N'Lamb - Bones', 50.7, 51.29, 21.69, 7.59, 39, 54, 1)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (86.1, 79.74, N'grams', N'Nut - Cashews, Whole, Raw', 33.69, 22.86, 55.28, 21.59, 40, 19, 26)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (67.55, 30.04, N'grams', N'The Pop Shoppe - Lime Rickey', 34.35, 94.11, 52.43, 36.47, 41, 56, 100)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (15.63, 7.07, N'grams', N'Bay Leaf Ground', 27.92, 11.86, 74.7, 94.03, 42, 89, 21)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (97.44, 99.35, N'grams', N'Chick Peas - Canned', 33.54, 40.28, 92.13, 80.33, 43, 32, 99)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (99.23, 93.92, N'grams', N'Cheese - Goat', 42.7, 22.31, 49.27, 59.08, 44, 14, 97)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (87.86, 55.37, N'grams', N'Wine - Red, Antinori Santa', 28.69, 18.68, 93.99, 4.31, 45, 98, 82)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (80.14, 73.76, N'grams', N'Shortbread - Cookie Crumbs', 38.23, 2.23, 82.71, 57, 46, 86, 37)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (35.36, 2.35, N'grams', N'Wine La Vielle Ferme Cote Du', 97.42, 63.5, 17.87, 29.72, 47, 53, 97)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (12.48, 81.85, N'grams', N'Tuna - Sushi Grade', 62.18, 95.6, 93.12, 2.38, 48, 71, 91)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (9.2, 54.3, N'grams', N'Durian Fruit', 2.98, 75.35, 67.49, 65.45, 49, 52, 11)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (85.3, 2.54, N'grams', N'Turkey - Breast, Boneless Sk On', 81.24, 25.64, 6.96, 94.1, 50, 44, 93)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (91.37, 43.65, N'grams', N'Cheese - Le Cheve Noir', 22.12, 72.98, 31.64, 48.9, 51, 74, 40)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (20.93, 53.81, N'grams', N'Juice - Clamato, 341 Ml', 63.35, 21.93, 44.26, 4.84, 52, 5, 97)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (88.63, 49.54, N'grams', N'Fish - Base, Bouillion', 70.93, 43.87, 93.96, 33.31, 53, 15, 55)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (27.55, 62.37, N'grams', N'Tequila Rose Cream Liquor', 2.41, 9.49, 2.25, 70.75, 54, 67, 50)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (80.6, 98.97, N'grams', N'French Pastry - Mini Chocolate', 2.36, 32.65, 93.04, 53.08, 55, 1, 40)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (49.45, 55.46, N'grams', N'Sea Bass - Fillets', 45.99, 41.86, 26.54, 70.37, 56, 99, 74)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (13.99, 76.83, N'grams', N'Muffin Mix - Morning Glory', 71.31, 37.32, 45, 91.49, 57, 69, 49)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (68.83, 74.16, N'grams', N'Appetizer - Chicken Satay', 23.21, 62.54, 29.04, 76.27, 58, 37, 70)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (49.17, 70.97, N'grams', N'Wine - Magnotta - Red, Baco', 65.17, 56.2, 80.62, 34.57, 59, 71, 25)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (49.75, 62, N'grams', N'Barramundi', 29, 91.57, 46.53, 54.56, 60, 89, 16)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (51.84, 34.77, N'grams', N'Turkey Tenderloin Frozen', 25.83, 22.11, 69.97, 6.01, 61, 18, 22)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (84.03, 22.54, N'grams', N'Cherries - Frozen', 49.95, 14.41, 77.31, 17.67, 62, 24, 17)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (53.8, 37.67, N'grams', N'Basil - Seedlings Cookstown', 42.27, 41.06, 90.04, 22.11, 63, 21, 26)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (71.65, 21.02, N'grams', N'Cheese - Swiss Sliced', 92.44, 18.69, 4.16, 65.25, 64, 51, 33)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (26.9, 22.72, N'grams', N'Soup - Knorr, Classic Can. Chili', 38.19, 68.16, 30.75, 55.47, 65, 51, 97)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (19.25, 96.05, N'grams', N'Banana', 45.01, 97.18, 83.08, 89.8, 66, 71, 89)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (54.49, 44.45, N'grams', N'Beer - Blue', 24.18, 62.06, 38.69, 49.53, 67, 99, 4)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (78.33, 71.19, N'grams', N'Cake Circle, Foil, Scallop', 77.67, 43.56, 45.25, 89.52, 68, 75, 60)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (79.01, 23.91, N'grams', N'Vol Au Vents', 75.28, 11.11, 67.13, 29.67, 69, 9, 68)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (71.15, 60.26, N'grams', N'Water - Spring 1.5lit', 54.27, 64.09, 59.35, 65.88, 70, 85, 71)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (13.82, 49.42, N'grams', N'Soup - Campbells, Classic Chix', 44.85, 29.06, 86.67, 87.61, 71, 44, 9)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (97.49, 33.08, N'grams', N'External Supplier', 36.83, 42.76, 35.95, 95.38, 72, 58, 6)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (63.59, 23.05, N'grams', N'Appetizer - Southwestern', 54.36, 82, 66.82, 49.8, 73, 2, 58)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (51.09, 83.91, N'grams', N'Bread - Sour Sticks With Onion', 97.33, 81.97, 51.01, 30.3, 74, 6, 41)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (13.33, 58.74, N'grams', N'Filter - Coffee', 79.78, 83.33, 88.74, 63.6, 75, 38, 22)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (66.04, 43.44, N'grams', N'Lamb - Whole, Fresh', 82.99, 88.4, 16.55, 79.61, 76, 92, 3)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (13.84, 9.66, N'grams', N'Mix Pina Colada', 70.96, 21.23, 85.01, 14.88, 77, 95, 52)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (22.75, 19.16, N'grams', N'Soup - Campbells Pasta Fagioli', 22.17, 87.15, 66.79, 10.07, 78, 40, 43)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (1.54, 58.19, N'grams', N'Almonds Ground Blanched', 44.31, 10.05, 57.43, 6.87, 79, 95, 49)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (28.51, 53.24, N'grams', N'Coffee - Almond Amaretto', 43.64, 41.61, 64, 27.95, 80, 14, 3)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (97.31, 54.48, N'grams', N'Table Cloth 90x90 White', 48.89, 38.58, 69.47, 83.38, 81, 41, 70)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (83.09, 78.73, N'grams', N'Oil - Margarine', 98.67, 64.98, 42.86, 50.04, 82, 6, 63)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (29.45, 92, N'grams', N'Energy - Boo - Koo', 62.68, 93.25, 48.77, 13.16, 83, 4, 77)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (18.53, 28.63, N'grams', N'Fudge - Chocolate Fudge', 63.62, 14.77, 89.85, 54.35, 84, 57, 93)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (92.88, 48.64, N'grams', N'Pate - Liver', 68.22, 75.02, 31.99, 33.51, 85, 41, 71)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (24.97, 86.73, N'grams', N'Tea - Black Currant', 68.2, 4.43, 31.58, 78.72, 86, 38, 29)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (21.16, 70.85, N'grams', N'Wine - Cave Springs Dry Riesling', 78.12, 37.6, 79.55, 30.94, 87, 88, 16)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (90.8, 75.37, N'grams', N'Halibut - Steaks', 89.34, 65.17, 69.08, 9.33, 88, 35, 61)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (28.03, 46.54, N'grams', N'Crackers - Water', 21.41, 81.67, 63.69, 94.84, 89, 7, 81)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (84.9, 18.01, N'grams', N'Syrup - Monin - Granny Smith', 32.95, 18.56, 56.62, 93.33, 90, 94, 52)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (16.22, 99.51, N'grams', N'Nutmeg - Ground', 78.06, 6.55, 53.48, 12.23, 91, 25, 48)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (61.45, 32.25, N'grams', N'Sauce - Soya, Dark', 96.63, 49.94, 76.45, 46.23, 92, 4, 77)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (4.56, 51.98, N'grams', N'Salami - Genova', 80.55, 28.67, 91.23, 1.67, 93, 85, 43)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (8.04, 99.58, N'grams', N'Cheese Cloth No 100', 6.66, 4.6, 11.13, 4.07, 94, 62, 59)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (3.32, 44.27, N'grams', N'Lemonade - Black Cherry, 591 Ml', 65.42, 18.36, 44.13, 86.89, 95, 79, 6)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (38.11, 68.36, N'grams', N'Dikon', 51.14, 74.46, 87.83, 75.14, 96, 1, 77)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (74.85, 28.56, N'grams', N'Lobster - Tail 6 Oz', 8.48, 47.15, 99.76, 86.3, 97, 9, 85)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (83, 71.02, N'grams', N'Rabbit - Saddles', 72.77, 94.4, 12.15, 99.57, 98, 88, 78)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (17.27, 82.57, N'grams', N'Lamb Shoulder Boneless Nz', 37.1, 27.01, 98.06, 66.43, 99, 52, 5)
INSERT [dbo].[Product] ([Quantity], [Threshold], [QuantityType], [Name], [Carbs], [Proteins], [Calories], [Fats], [id_Product], [fk_Dietid_Diet], [fk_Recipeid_Recipe]) VALUES (44.44, 35.64, N'grams', N'Ham - Cooked', 89.85, 94.14, 20.63, 67.22, 100, 57, 27)
GO
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'ipsum dolor', N'purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut', 1)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'potenti in', N'morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra', 2)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'lectus', N'aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas', 3)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'habitasse', N'et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum', 4)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'lacinia aenean', N'ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor', 5)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'magna', N'erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget', 6)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'sociis', N'ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor', 7)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'consectetuer adipiscing', N'penatibus et magnis dis parturient montes nascetur ridiculus mus etiam', 8)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'lobortis', N'elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis', 9)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'rutrum at', N'libero nullam sit amet turpis elementum ligula vehicula consequat morbi', 10)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'sed magna', N'posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non', 11)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'volutpat convallis', N'nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo', 12)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'vestibulum', N'non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem', 13)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'nunc', N'orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum', 14)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'interdum', N'ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc', 15)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'sapien non', N'praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum', 16)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'velit', N'mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis', 17)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'ante', N'maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus', 18)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'cursus urna', N'primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur', 19)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'nec molestie', N'parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque', 20)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'tempus sit', N'venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet', 21)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'nisi venenatis', N'sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris', 22)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'morbi non', N'ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit', 23)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'accumsan felis', N'nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam', 24)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'nulla quisque', N'est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia', 25)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'dui', N'at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna', 26)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'eget nunc', N'dui maecenas tristique est et tempus semper est quam pharetra magna', 27)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'nunc', N'luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor', 28)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'volutpat in', N'quisque arcu libero rutrum ac lobortis vel dapibus at diam nam tristique', 29)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'et', N'libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante', 30)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'nec', N'vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum', 31)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'vel', N'tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum', 32)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'dui', N'porttitor id consequat in consequat ut nulla sed accumsan felis ut at dolor quis odio', 33)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'sapien', N'phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae', 34)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'odio', N'massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim', 35)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'eget tincidunt', N'hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie', 36)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'eget', N'pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum', 37)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'viverra', N'eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium', 38)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'id', N'etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean', 39)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'fringilla', N'pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc', 40)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'turpis', N'ligula in lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum', 41)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'tortor', N'vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in', 42)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'semper porta', N'id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu', 43)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'ligula', N'sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo', 44)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'volutpat in', N'dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat', 45)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'auctor gravida', N'metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet', 46)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'mauris lacinia', N'mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis a pede posuere', 47)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'eget', N'quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan', 48)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'duis', N'nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper', 49)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'eu magna', N'ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at', 50)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'consectetuer adipiscing', N'primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan', 51)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'aenean', N'vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque', 52)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'morbi', N'augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae', 53)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'in', N'eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu', 54)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'suscipit', N'cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus', 55)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'lobortis sapien', N'lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in', 56)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'a ipsum', N'leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis', 57)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'pede lobortis', N'massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus', 58)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'dis parturient', N'in est risus auctor sed tristique in tempus sit amet sem', 59)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'pretium', N'magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in', 60)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'faucibus orci', N'tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra', 61)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'odio', N'sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel', 62)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'vel', N'et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et', 63)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'quis', N'amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim', 64)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'ligula', N'vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in', 65)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'eu', N'et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit', 66)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'elit proin', N'elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in', 67)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'at velit', N'ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien', 68)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'est', N'habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit', 69)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'turpis nec', N'sem praesent id massa id nisl venenatis lacinia aenean sit', 70)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'quis', N'lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus', 71)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'in hac', N'nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula', 72)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'vel', N'elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in', 73)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'scelerisque', N'nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo', 74)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'hac', N'nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo', 75)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'nulla', N'rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed', 76)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'lorem vitae', N'velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis', 77)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'lorem id', N'nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel', 78)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'ipsum', N'diam vitae quam suspendisse potenti nullam porttitor lacus at turpis', 79)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'tristique', N'at nulla suspendisse potenti cras in purus eu magna vulputate', 80)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'quisque', N'cubilia curae nulla dapibus dolor vel est donec odio justo', 81)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'rhoncus', N'nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris', 82)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'vestibulum', N'cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam', 83)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'nec', N'vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae', 84)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'quis justo', N'cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum', 85)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'interdum', N'aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu', 86)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'sit amet', N'luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum', 87)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'nulla sed', N'lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque', 88)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'porta volutpat', N'viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis', 89)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'lectus', N'odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue', 90)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'bibendum morbi', N'pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci', 91)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'tincidunt eget', N'elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus', 92)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'diam id', N'neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus', 93)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'in', N'consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac', 94)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'faucibus orci', N'aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque', 95)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'leo', N'pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna', 96)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'pede posuere', N'ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non', 97)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'feugiat', N'ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor', 98)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'justo', N'maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut', 99)
INSERT [dbo].[Recipe] ([Name], [Description], [id_Recipe]) VALUES (N'quis', N'sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas', 100)
GO
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (64.08, N'grams', 1, 99, 56)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (22.01, N'grams', 2, 69, 64)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (55.71, N'grams', 3, 43, 50)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (90.84, N'grams', 4, 81, 11)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (80.66, N'grams', 5, 92, 85)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (66.25, N'grams', 6, 16, 33)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (77.25, N'grams', 7, 75, 3)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (1.49, N'grams', 8, 59, 61)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (61.04, N'grams', 9, 35, 98)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (86.07, N'grams', 10, 59, 52)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (98.29, N'grams', 11, 20, 75)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (90.38, N'grams', 12, 82, 23)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (43.38, N'grams', 13, 12, 58)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (33.64, N'grams', 14, 80, 67)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (9.07, N'grams', 15, 50, 60)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (33.43, N'grams', 16, 38, 81)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (76.98, N'grams', 17, 20, 30)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (89.78, N'grams', 18, 71, 31)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (67.37, N'grams', 19, 67, 98)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (74.45, N'grams', 20, 87, 44)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (32.42, N'grams', 21, 89, 18)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (96.21, N'grams', 22, 14, 40)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (53.07, N'grams', 23, 50, 82)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (69.72, N'grams', 24, 27, 71)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (47.47, N'grams', 25, 69, 17)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (63.98, N'grams', 26, 69, 60)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (95.8, N'grams', 27, 95, 3)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (6.64, N'grams', 28, 23, 9)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (63.87, N'grams', 29, 99, 15)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (15.12, N'grams', 30, 40, 86)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (49.98, N'grams', 31, 57, 34)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (55.95, N'grams', 32, 25, 67)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (64.44, N'grams', 33, 89, 44)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (82.83, N'grams', 34, 9, 88)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (73.69, N'grams', 35, 27, 90)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (18.91, N'grams', 36, 69, 71)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (89.98, N'grams', 37, 42, 21)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (53.75, N'grams', 38, 69, 91)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (18.86, N'grams', 39, 100, 52)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (65.6, N'grams', 40, 39, 81)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (17.83, N'grams', 41, 67, 73)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (54.87, N'grams', 42, 50, 12)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (20.77, N'grams', 43, 69, 80)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (56.73, N'grams', 44, 1, 4)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (2.14, N'grams', 45, 95, 1)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (9.43, N'grams', 46, 5, 25)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (7.22, N'grams', 47, 5, 97)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (29.25, N'grams', 48, 5, 38)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (48.91, N'grams', 49, 50, 100)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (14.34, N'grams', 50, 12, 9)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (13.81, N'grams', 51, 79, 88)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (35.43, N'grams', 52, 35, 11)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (28.82, N'grams', 53, 27, 38)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (68.99, N'grams', 54, 60, 41)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (72.31, N'grams', 55, 97, 92)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (66.5, N'grams', 56, 7, 9)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (11.54, N'grams', 57, 54, 5)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (22.9, N'grams', 58, 28, 49)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (24.22, N'grams', 59, 30, 52)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (62.61, N'grams', 60, 99, 65)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (30.87, N'grams', 61, 34, 60)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (11.76, N'grams', 62, 8, 70)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (58.59, N'grams', 63, 24, 79)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (79.3, N'grams', 64, 30, 96)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (56.64, N'grams', 65, 40, 66)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (31.08, N'grams', 66, 59, 95)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (9.86, N'grams', 67, 16, 67)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (53.09, N'grams', 68, 85, 77)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (68.96, N'grams', 69, 57, 41)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (62.65, N'grams', 70, 69, 28)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (18.87, N'grams', 71, 59, 78)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (46.87, N'grams', 72, 48, 70)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (62.56, N'grams', 73, 43, 14)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (97.69, N'grams', 74, 95, 85)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (48.3, N'grams', 75, 56, 87)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (34.52, N'grams', 76, 100, 11)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (35.6, N'grams', 77, 86, 62)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (18.41, N'grams', 78, 34, 1)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (78.78, N'grams', 79, 48, 98)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (87.4, N'grams', 80, 6, 22)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (16.13, N'grams', 81, 22, 100)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (34.48, N'grams', 82, 96, 69)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (46.36, N'grams', 83, 57, 20)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (60.87, N'grams', 84, 45, 37)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (2.38, N'grams', 85, 9, 8)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (25.2, N'grams', 86, 58, 59)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (55.28, N'grams', 87, 71, 99)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (83.32, N'grams', 88, 78, 5)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (52.5, N'grams', 89, 57, 31)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (96.47, N'grams', 90, 74, 72)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (61.57, N'grams', 91, 50, 7)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (72.21, N'grams', 92, 29, 15)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (47.13, N'grams', 93, 53, 89)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (51.1, N'grams', 94, 74, 74)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (71.83, N'grams', 95, 42, 34)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (38.21, N'grams', 96, 12, 50)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (48.44, N'grams', 97, 80, 19)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (60.13, N'grams', 98, 85, 1)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (98.19, N'grams', 99, 84, 14)
INSERT [dbo].[RecipeProduct] ([Quantity], [Units], [id_RecipeProduct], [fk_Recipeid_Recipe], [fk_Productid_Product]) VALUES (82.78, N'grams', 100, 100, 57)
GO
ALTER TABLE [dbo].[DietProduct]  WITH CHECK ADD  CONSTRAINT [may_have4] FOREIGN KEY([fk_Productid_Product])
REFERENCES [dbo].[Product] ([id_Product])
GO
ALTER TABLE [dbo].[DietProduct] CHECK CONSTRAINT [may_have4]
GO
ALTER TABLE [dbo].[DietProduct]  WITH CHECK ADD  CONSTRAINT [may_have6] FOREIGN KEY([fk_Dietid_Diet])
REFERENCES [dbo].[Diet] ([id_Diet])
GO
ALTER TABLE [dbo].[DietProduct] CHECK CONSTRAINT [may_have6]
GO
ALTER TABLE [dbo].[FridgeUserProduct]  WITH CHECK ADD  CONSTRAINT [may_have] FOREIGN KEY([fk_FridgeUserid_FridgeUser])
REFERENCES [dbo].[FridgeUser] ([id_FridgeUser])
GO
ALTER TABLE [dbo].[FridgeUserProduct] CHECK CONSTRAINT [may_have]
GO
ALTER TABLE [dbo].[FridgeUserProduct]  WITH CHECK ADD  CONSTRAINT [may_have3] FOREIGN KEY([fk_Productid_Product])
REFERENCES [dbo].[Product] ([id_Product])
GO
ALTER TABLE [dbo].[FridgeUserProduct] CHECK CONSTRAINT [may_have3]
GO
ALTER TABLE [dbo].[Offer]  WITH CHECK ADD  CONSTRAINT [may_have5] FOREIGN KEY([fk_Productid_Product])
REFERENCES [dbo].[Product] ([id_Product])
GO
ALTER TABLE [dbo].[Offer] CHECK CONSTRAINT [may_have5]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([fk_Dietid_Diet])
REFERENCES [dbo].[Diet] ([id_Diet])
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([fk_Recipeid_Recipe])
REFERENCES [dbo].[Recipe] ([id_Recipe])
GO
ALTER TABLE [dbo].[SubscribedProduct]  WITH CHECK ADD FOREIGN KEY([fk_FridgeUserid_FridgeUser])
REFERENCES [dbo].[FridgeUser] ([id_FridgeUser])
GO
ALTER TABLE [dbo].[SubscribedProduct]  WITH CHECK ADD FOREIGN KEY([fk_Productid_Product])
REFERENCES [dbo].[Product] ([id_Product])
GO
