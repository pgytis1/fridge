import { createStore, combineReducers, applyMiddleware, compose } from 'redux';

import * as auth from './auth';

const makeReducer = () => combineReducers({
    auth: auth.reducer
});



const makeMiddlewares = () => {
    const middlewares = [];
    if (process.env.NODE_ENV === 'development') {
        const { createLogger } = require('redux-logger');
        const logger = createLogger({ collapsed: true });
        middlewares.push(logger);
    }

    return middlewares;
};

export default () => {
    const reducer = makeReducer();
    const middlewares = makeMiddlewares();
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(reducer, composeEnhancers(
        applyMiddleware(...middlewares)
    ));
    return store;
};
