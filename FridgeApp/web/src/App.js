import React, { Component } from 'react';
import { Provider } from 'react-redux';
import * as api from './api';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'


// **
import ProductManagementPage from './components/ProductManagementPage'
import ProductCreateForm from './components/ProductCreateForm';
import ProductEditForm from './components/ProductEditForm';
import AdvertisementManagementPage from './components/AdvertisementManagementPage';
import AdvertisementCreateForm from './components/AdvertisementCreateForm';
import AdvertisementEditForm from './components/AdvertisementEditForm';
import AdvertisementPage from './components/AdvertisementPage';
import RecipesManagementPage from './components/RecipesManagementPage';
import RecipesCreateForm from './components/RecipesCreateForm';
import RecipeEditForm from './components/RecipeEditForm';
import StatisticsPage from './components/StatisticsPage';
import GenerateRecipePage from './components/GenerateRecipePage';
import ProductsNeedPage from './components/ProductsNeedPage';
import DiscountsPage from './components/Gallery';
import DietsPage from './components/DietsPage';
// **
import AdminNavigation from './components/AdminNavigation';

class App extends Component {
  componentDidMount() {
    const data = api.auth.login();
    console.warn(data);
  }

  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <Router>
          <AdminNavigation>
            <Switch>
              <Route exact path="/" component={() => <div>home</div>} />
              <Route path="/products" exact component={ProductManagementPage} />
              <Route path="/products/create" exact component={ProductCreateForm} />
              <Route path="/products/edit/:id" exact component={ProductEditForm} />
              <Route path="/recipes" exact component={RecipesManagementPage} />
              <Route path="/recipes/create" exact component={RecipesCreateForm} />
              <Route path="/recipes/edit/:id" exact component={RecipeEditForm} />
              <Route path="/advertisements" exact component={AdvertisementManagementPage} />
              <Route path="/advertisements/create" exact component={AdvertisementCreateForm} />
              <Route path="/advertisements/edit/:id" exact component={AdvertisementEditForm} />
              <Route path="/statisticsPage" exact component={StatisticsPage} />
              <Route path="/generateRecipePage" exact component={GenerateRecipePage} />
              <Route path="/productsNeedPage" exact component={ProductsNeedPage} />
              <Route path="/discounts" exact component={DiscountsPage} />
              <Route path="/diets" exact component={DietsPage} />
            </Switch>
          </AdminNavigation>
        </Router>
      </Provider>
    );
  }
}

export default App;
