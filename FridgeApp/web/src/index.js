import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import configureStore from './configureStore';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/bootstrap/dist/js/bootstrap.js';


const store = configureStore();

ReactDOM.render(
    <App store={store} />,
    document.getElementById('root')
);
