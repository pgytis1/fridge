import * as superagent from 'superagent';

const domain = 'http://localhost:60278/';

const request = async (method, url, data) => {
    const jwt = ''
    switch (method) {
        case 'GET':
            return superagent.get(domain + url).set('Authorization', `Bearer ${jwt}`).then(res => res.body);
        case 'POST':
            return superagent.post(domain + url).send(data).set('Authorization', `Bearer ${jwt}`).then(res => res.body);
        case 'PUT':
            return superagent.put(domain + url).send(data).set('Authorization', `Bearer ${jwt}`).then(res => res.body);
        case 'DELETE':
            return superagent.delete(domain + url).send(data).set('Authorization', `Bearer ${jwt}`).then(res => res.body);
        default:
            return;
    }
};

export default {
    get: (url) => request('GET', url),
    post: (url, data) => request('POST', url, data),
    put: (url, data) => request('PUT', url, data),
    delete: (url) => request('DELETE', url)
};