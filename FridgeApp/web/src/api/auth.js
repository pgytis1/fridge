import apiClient from './apiClient';

export const login = (email, password) => apiClient
    .get('api/values', { email, password });
