import React from 'react';

class ProductCreateForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "",
            carbs: "",
            proteins: "",
            fats: "",
            calories: "",
        }
    }

    handleNameChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            name: val
        }));
        console.warn(this.state);
    }

    handleCarbsChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            carbs: val
        }))
        console.warn(this.state);

    }

    handleProteinsChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            proteins: val
        }))
        console.warn(this.state);
    }

    handleFatsChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            fats: val
        }));
        console.warn(this.state);
    }

    handleCaloriesChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            calories: val
        }));
        console.warn(this.state);
    }

    handleSave = () => {

    }

    render() {
        return (
            <div className="p-5">
                <h2>Sukurti naują produktą</h2>
                <form>
                    <div className="form-group">
                        <label>Pavadinimas</label>
                        <input type="text" className="form-control" placeholder="Įveskite pavadinimą" onChange={this.handleNameChange} value={this.state.name} />
                    </div>
                    <div className="form-group">
                        <label>Angliavandeniai</label>
                        <input type="text" className="form-control" placeholder="Įveskite angliavandenių kiekį" onChange={this.handleCarbsChange} value={this.state.carbs} />
                    </div>
                    <div className="form-group">
                        <label>Baltymai</label>
                        <input type="text" className="form-control" placeholder="Įveskite baltymų kiekį" onChange={this.handleProteinsChange} value={this.state.proteins} />
                    </div>
                    <div className="form-group">
                        <label>Riebalai</label>
                        <input type="text" className="form-control" placeholder="Įveskite riebalų kiekį" onChange={this.handleFatsChange} value={this.state.fats} />
                    </div>
                    <div className="form-group">
                        <label>Kalorijos</label>
                        <input type="text" className="form-control" placeholder="Įveskite kalorijų kiekį" onChange={this.handleCaloriesChange} value={this.state.calories} />
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={this.handleSave}>Saugoti</button>
                </form>
            </div>
        )
    }
}

export default ProductCreateForm;