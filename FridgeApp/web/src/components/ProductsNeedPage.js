import React from 'react';
import { NavLink } from 'react-router-dom'
class ProductsNeedPage extends React.Component {
    constructor() {
        super();
        this.state = {
            products: [
                {
                    id: 1,
                    name: "Bananas",
                    carbs: 1,
                    proteins: 1,
                    fats: 1,
                    calories: 1,
                },
                {
                    id: 2,
                    name: "Agurkas",
                    carbs: 1,
                    proteins: 1,
                    fats: 1,
                    calories: 1,
                }
            ]
        }
    }

    async componentDidMount(){
        // const stats = await this.getData();
        let response = await fetch('http://localhost:61664/api/GetAllNeedProducts');
        let data = await response.json();
        console.log(data);
        this.setState({products:data});
    }

    // getData = async () => {
    //      let response = await fetch('http://localhost:61664/api/ProductsNeedPage');
    //      let data = await response.json();
    //      console.warn(data)
    //      return data;
    // }
    handleCaloriesChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            calories: val
        }));
        console.warn(this.state);
    }
    
    renderNeedAllProducts(){
        const listItems = this.state.products.map((prod) =>
         <li key={prod.id} >
            <NavLink to={"/products/edit/"+prod.id} className="nav-link" activeClassName='nav-link active'>{prod.name}</NavLink>
        {/* {prod.name} {prod.kiekis} */}
            </li>
        )

        // <li key={prod.id} >
        //     <NavLink to='{prod.link}' className="nav-link" activeClassName='nav-link active'>{prod.name}</NavLink>
        // {prod.name} {prod.kiekis}
        //     </li>
        // )
        return listItems;
    }

    render() {
        return (
            <div className="container">
                <h3>Trūkstančių produktų sarašas</h3>
                <ul>
                    {this.renderNeedAllProducts()}
                </ul>
            </div>
            )
    }
}

export default ProductsNeedPage;