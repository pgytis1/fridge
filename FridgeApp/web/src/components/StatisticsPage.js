import React from 'react';
import { Bar } from 'react-chartjs-2';


const data = {
    labels: ['Riebalai', 'Baltymai', 'Angliavandeniai', 'kalorijos'],
    datasets: [
        {
            label: 'Šviežiausias produktas',
            backgroundColor: '#FF6384',
            // borderColor: 'rgba(255,99,132,1)',
            // borderWidth: 1,
            // hoverBackgroundColor: '#FF6384',
            // hoverBorderColor: 'rgba(255,99,132,1)',
            data: [65, 59, 80]
        },
        {
            label: 'Kaloringiausias produktas',
            backgroundColor: '#36A2EB',
            // borderColor: 'rgba(255,99,132,1)',
            // borderWidth: 1,
            // hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            // hoverBorderColor: 'rgba(255,99,132,1)',
            data: [65, 59, 80]
        },
        {
            label: 'Seniausias produktas',
            backgroundColor: '#FFCE56',
            // borderColor: 'rgba(255,99,132,1)',
            // borderWidth: 1,
            // hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            // hoverBorderColor: 'rgba(255,99,132,1)',
            data: [65, 59, 80]
        }
    ]
};

class StatisticsPage extends React.Component {
     constructor() {
        super();
        this.state = {
            freshProduct: 
            {
                name: "◌",
                carbs: 0,
                proteins: 0,
                fats: 0,
                calories: 0,
            },
            calorificProduct:
            {
                name: "◌",
                carbs: 0,
                proteins: 0,
                fats: 0,
                calories:0,
            },
            oldProduct:
            {
                name: "◌",
                carbs: 0,
                proteins: 0,
                fats:0,
                calories: 0,
            },
        }

        //const stats = await this.getData();
       // this.setState(stats);
        //this.reload();
       // console.log(stats);

    }

    reload = () => {
         //'Riebalai', 'Baltymai', 'Angliavandeniai','kalorijos'
        //Šviežiausias
        console.log(this.state);

        data.datasets[0].label = "Šviežiausias produktas" + " "+ this.state.freshProduct.name
        data.datasets[0].data =
            [
                this.state.freshProduct.fats,
                this.state.freshProduct.proteins,
                this.state.freshProduct.carbs,
                this.state.freshProduct.calories,
            ]
        //kaloringiausias
        data.datasets[1].label = "Kaloringiausias produktas" + " " + this.state.calorificProduct.name
        data.datasets[1].data =
            [
                this.state.calorificProduct.fats,
                this.state.calorificProduct.proteins,
                this.state.calorificProduct.carbs,
                this.state.calorificProduct.calories,
            ]
        //seniausias
        data.datasets[2].label = "Seniausias produktas" + " " + this.state.oldProduct.name
        data.datasets[2].data =
            [
                this.state.oldProduct.fats,
                this.state.oldProduct.proteins,
                this.state.oldProduct.carbs,
                this.state.oldProduct.calories,
            ]
    }
    async componentDidMount(){
        const stats = await this.getData();
        // this.setState(stats);
        // await this.setStateAsync(stats)
        this.setState(stats)
        this.reload();
        console.log(stats);
    }

      getData = async () => {
         let response = await fetch('http://localhost:61664/api/statistics');
         let data = await response.json();
         return data;
      }

    handleCaloriesChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            calories: val
        }));
        console.warn(this.state);
    }
    IfProductExists = (product) => {
        return product !== null ? product.name : "Nėra";
    }

    render() {
        return (
            <div className="container">
                <div className="jumbotron">
                    <h1>Statistika</h1>
                    <ul>
                        <li>Šviežiausias produktas {this.IfProductExists(this.state.freshProduct)}</li>
                        <li>Kaloringiausias produktas {this.IfProductExists(this.state.calorificProduct)}</li>
                        <li>Seniausias Produtkas  {this.IfProductExists(this.state.oldProduct)}</li>
                    </ul>
                </div>
                <div className="container">
                    <Bar data={data}
                        width={50}
                        height={400}
                        options={{ maintainAspectRatio: false }} />
                </div>
            </div>)
    }
}

export default StatisticsPage;