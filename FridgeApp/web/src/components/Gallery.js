import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { css, StyleSheet } from 'aphrodite/no-important';
import Modal from 'react-modal';

const customStyles = {
	content: {
		top: '50%',
		left: '50%',
		right: 'auto',
		bottom: 'auto',
		marginRight: '-50%',
		transform: 'translate(-50%, -50%)'
	}
};

// class Gallery extends Component {
// 	constructor() {
// 		super();
// 		this.state = {
// 			modalIsOpen: false,
// 		}
// 	}
// 	openModal = (x) => {
// 		this.setState(() => ({
// 			modalIsOpen: true,
// 		}));
// 	}

// 	closeModal = () => {
// 		this.setState(() => ({
// 			modalIsOpen: false,
// 		}))
// 	}

// 	render() {
// 		return (
// 			<div>
// 				<ul class="nav nav-tabs">
// 					<li class="nav-item">
// 						<a class="nav-link active" href="#">Akcijos</a>
// 					</li>
// 					<li class="nav-item">
// 						<a class="nav-link" href="#">Prenumeruojamos akcijos</a>
// 					</li>
// 				</ul>
// 				<hr />
// 				<span class="badge badge-success ml-1">Daržovės</span>
// 				<span class="badge badge-success ml-2">Vaisiai</span>
// 				<button type="button" class="btn btn-warning btn-sm float-right mr-2" onClick={this.openModal}>Prenumeruoti akcijas</button>
// 				<hr />

// 				<img src={require('./1.jpg')} />
// 				<img src={require('../images/2.PNG')} />
// 				<span className="pl-2">
// 					<img src={require('../images/3.PNG')} />
// 				</span>

// 				<span className="pl-2">
// 					<img src={require('../images/5.PNG')} />
// 				</span>
// 				<hr />
// 				<img src={require('./1.jpg')} />
// 				<img src={require('../images/2.PNG')} />
// 				<span className="pl-2">
// 					<img src={require('../images/3.PNG')} />
// 				</span>

// 				<span className="pl-2">
// 					<img src={require('../images/5.PNG')} />
// 				</span>
// 				<hr />
// 				<img src={require('./1.jpg')} />
// 				<img src={require('../images/2.PNG')} />
// 				<span className="pl-2">
// 					<img src={require('../images/3.PNG')} />
// 				</span>

// 				<span className="pl-2">
// 					<img src={require('../images/5.PNG')} />
// 				</span>
// 				<Modal
// 					isOpen={this.state.modalIsOpen}
// 					onRequestClose={this.closeModal}
// 					contentLabel="Example Modal"
// 					style={customStyles}
// 				>
// 					{this.state.modalContent && this.state.modalContent.description}
// 					<br />
// 					<br />
// 					<button className="btn btn-danger" onClick={this.closeModal}>Uždaryti</button>
// 				</Modal>
// 			</div >
// 		);
// 	}
// }

// export default Gallery;

class Gallery extends Component {
	constructor() {
		super();
		this.state = {
			modalIsOpen: false,
		}
	}
	openModal = (x) => {
		this.setState(() => ({
			modalIsOpen: true,
		}));
	}

	closeModal = () => {
		this.setState(() => ({
			modalIsOpen: false,
		}))
	}

	render() {
		return (
			<div>
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link" href="#">Akcijos</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="#">Prenumeruojamos akcijos</a>
					</li>
				</ul>
				<hr />
				<span class="badge badge-success ml-1">Daržovės</span>
				<button type="button" class="btn btn-warning btn-sm float-right mr-2" onClick={this.openModal}>Prenumeruoti akcijas</button>
				<hr />

				<img src={require('./1.jpg')} />
				<img src={require('../images/2.PNG')} />
				<span className="pl-2">
					<img src={require('../images/3.PNG')} />
				</span>


				<img src={require('./1.jpg')} />
				<hr />

				<img src={require('../images/2.PNG')} />
				<span className="pl-2">
					<img src={require('../images/3.PNG')} />
				</span>

				<img src={require('./1.jpg')} />
				<img src={require('../images/2.PNG')} />
				<hr />

				<span className="pl-2">
					<img src={require('../images/3.PNG')} />
				</span>

				<Modal
					isOpen={this.state.modalIsOpen}
					onRequestClose={this.closeModal}
					contentLabel="Example Modal"
					style={customStyles}
				>
					<h4>Prenumeruoti akcijas šioms prekėms: </h4>
					<div class="form-check">
						<input type="checkbox" class="form-check-input" id="exampleCheck1" checked={true} />
						<label className="form-check-label" for="exampleCheck1">Daržovės</label>
					</div>
					<div class="form-check">
						<input type="checkbox" class="form-check-input" id="exampleCheck1" />
						<label className="form-check-label" for="exampleCheck1">Vaisiai</label>
					</div>
					<br />
					<br />
					<button className="btn btn-success mr-3" onClick={this.closeModal}>Prenumeruoti</button>
					<button className="btn btn-danger" onClick={this.closeModal}>Uždaryti</button>
				</Modal>
			</div >
		);
	}
}

export default Gallery;