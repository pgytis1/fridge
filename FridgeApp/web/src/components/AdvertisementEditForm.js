import React from 'react';


class AdvertisementEditForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "",
            description: "",
            date: "",
            isActive: false
        }
    }

    componentDidMount(){
        console.warn(+this.props.match.params.id);
    }

    handleNameChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            name: val
        }));
        console.warn(this.state);
    }

    handleDescriptionChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            description: val
        }))
        console.warn(this.state);

    }

    handleDateChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            date: val
        }))
        console.warn(this.state);
    }

    handleIsActiveChange = () => {
        this.setState(() => ({
            isActive: !this.state.isActive
        }));
        console.warn(this.state);
    }

    handleSave = () => {

    }

    render() {
        return (
            <div className="p-5">
                <h2>Redaguoti skelbimą</h2>
                <form>
                    <div className="form-group">
                        <label>Pavadinimas</label>
                        <input type="text" className="form-control" placeholder="Įveskite pavadinimą" onChange={this.handleNameChange} value={this.state.name} />
                    </div>
                    <div className="form-group">
                        <label>Aprašymas</label>
                        <input type="text" className="form-control" placeholder="Įveskite aprašymą" onChange={this.handleDescriptionChange} value={this.state.description} />
                    </div>
                    <div className="form-group">
                        <label>Data</label>
                        <input type="date" className="form-control" placeholder="Įveskite datą" onChange={this.handleDateChange} value={this.state.date} />
                    </div>
                    <div className="form-group form-check">
                        <input type="checkbox" className="form-check-input" id="exampleCheck1" onClick={this.handleIsActiveChange} value={this.state.isActive}/>
                        <label className="form-check-label">Ar aktyvus</label>
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={this.handleSave}>Saugoti</button>
                </form>
            </div>
        )
    }
}

export default AdvertisementEditForm;