import React from 'react';

class GenerateRecipePage extends React.Component {
    constructor() {
        super();
        this.state = {
            name : "Cepelinai",
            description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            products: [
                {
                    id: 1,
                    name:"Pomidoras",
                    quantity: 0,
                    unit: "vnt"
                },
                {
                    id: 2,
                    name:"Agurkas",
                    quantity: 1,
                    unit: "vnt"
                },
                {
                    id: 3,
                    name:"Citrina",
                    quantity: 1,
                    unit: "vnt"
                },
                {
                    id: 4,
                    name:"Bulve",
                    quantity: 2,
                    unit: "vnt"
                },
                {
                    id: 5,
                    name:"Druska",
                    quantity: 1,
                    unit: "vnt"
                },
            ],
        }

    
    }

    async componentDidMount(){
        // const stats = await this.getData();
        console.log("---");
        let response = await fetch('http://localhost:61664/api/CreateRecipe');
        let data = await response.json();
        console.log("---");
        console.log(data);

        this.setState(data);
    }

    RenderTableRow(product)
    {
        return (
            <tr>
                <th scope="row">{product.id}</th>
                <td>{product.name}</td>
                <td>{product.quantity}</td>
                <td>{product.units}</td>
            </tr>
        );
    }
    RenderTable()
    {
        return(
            <div>
                <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Pavadinimas</th>
                        <th scope="col">Kiekis</th>
                        <th scope="col">Vienetai</th>
                    </tr>
                </thead>
                <tbody>
                     {this.state.products.map( (product) =>this.RenderTableRow(product) )}
                </tbody>
                </table>
            </div>
        );
    }

    handleCaloriesChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            calories: val
        }));
        console.warn(this.state);
    }

    render() {
        return (
            <div className="container">
                <h3>{this.state.name}</h3>
                <div>
                  {this.state.description}
                </div>
                
                <div>
                  {this.RenderTable()}
                </div>
            </div>
            )
    }


}

export default GenerateRecipePage;