import React from 'react';

class ProductManagementPage extends React.Component {
    constructor() {
        super();
        this.state = {
            products: [
                {
                    id: 1,
                    name: "Duona",
                    carbs: 12,
                    proteins: 14,
                    fats: 10,
                    calories: 100,
                },
                {
                    id: 2,
                    name: "Pienas",
                    carbs: 1,
                    proteins: 10,
                    fats: 5,
                    calories: 70,
                }
            ]
        }
    }

    handleDeleteProduct = (index) => {
        console.warn('del');
    }

    handleEdit = (id) => {
        this.props.history.push(`/products/edit/${id}`)
    }

    handleAdd = () => {
        this.props.history.push(`/products/create`)
    }

    render() {
        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Pavadinimas</th>
                            <th scope="col">Angliavandeniai</th>
                            <th scope="col">Baltymai</th>
                            <th scope="col">Riebalai</th>
                            <th scope="col">Kalorijos</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.products.map((x, i) => (
                            <tr key={i}>
                                <th scope="row">{i}</th>
                                <td>{x.name}</td>
                                <td>{x.carbs}</td>
                                <td>{x.proteins}</td>
                                <td>{x.fats}</td>
                                <td>{x.calories}</td>
                                <td>
                                    <button type="button" className="btn btn-sm btn-success ml-2" onClick={() => this.handleEdit(x.id)}>Redaguoti</button>
                                    <button type="button" className="btn btn-sm btn-danger ml-2" onClick={() => this.handleDeleteProduct(i)}>Trinti</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button type="submit" className="btn btn-primary ml-3" onClick={() => this.handleAdd()}>Pridėti</button>
            </div>
        )
    }
}

export default ProductManagementPage;