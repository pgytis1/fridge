import React from 'react';
import Modal from 'react-modal';
import { withRouter } from 'react-router'

class AdvertisementManagementPage extends React.Component {
    constructor() {
        super();
        this.state = {
            modalIsOpen: false,
            advertisements: [
                {
                    id: 1,
                    name: "1",
                    description: "asd",
                    date: new Date(),
                    isActive: false,
                },
                {
                    id: 1,
                    name: "1",
                    description: "123",
                    date: new Date(),
                    isActive: true,
                }
            ],
            modalContent: null
        }
    }

    handleDeleteAdvertisement = (index) => {
        console.warn('del');
    }

    openModal = (x) => {
        this.setState(() => ({
            modalIsOpen: true,
            modalContent: x
        }));
    }

    closeModal = () => {
        this.setState(() => ({
            modalIsOpen: false,
        }))
    }

    handleEdit = (id) => {
        this.props.history.push(`/advertisements/edit/${id}`)
    }

    handleAdd = () => {
        this.props.history.push(`/advertisements/create`)
    }

    render() {
        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Pavadinimas</th>
                            <th scope="col">Aprašymas</th>
                            <th scope="col">Data</th>
                            <th scope="col">Ar aktyvus</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.advertisements.map((x, i) => (
                            <tr key={i}>
                                <th scope="row">{i}</th>
                                <td>{x.name}</td>
                                <td>{x.description.slice(0, 10)}
                                    <button type="button" className="btn btn-sm btn-default ml-2" onClick={() => this.openModal(x)}>...</button>
                                </td>
                                <td>{x.date.toLocaleDateString()}</td>
                                <td>{x.isActive ? 'Taip' : 'Ne'}</td>
                                <td>
                                    <button type="button" className="btn btn-sm btn-success ml-2" onClick={() => this.handleEdit(x.id)}>Redaguoti</button>
                                    <button type="button" className="btn btn-sm btn-danger ml-2" onClick={() => this.handleDeleteAdvertisement(i)}>Trinti</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button type="submit" className="btn btn-primary ml-3" onClick={() => this.handleAdd()}>Pridėti</button>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onRequestClose={this.closeModal}
                    contentLabel="Example Modal"
                >
                    {this.state.modalContent && this.state.modalContent.description}
                    <br />
                    <br />
                    <button className="btn btn-danger" onClick={this.closeModal}>Uždaryti</button>
                </Modal>
            </div>
        )
    }
}

export default withRouter(AdvertisementManagementPage);