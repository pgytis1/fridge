import React from 'react';
import Modal from 'react-modal';

class DietsPage extends React.Component {
    constructor() {
        super();
        this.state = {
            modalIsOpen: false,
            modal2IsOpen: false,
            recipes: [
                {
                    id: 1,
                    name: "1",
                    description: "123",
                    products: [
                        {
                            id: 1,
                            quantity: 0,
                            unit: "vnt"
                        },
                        {
                            id: 1,
                            quantity: 0,
                            unit: "vnt"
                        },
                    ],
                },
                {
                    id: 2,
                    name: "1",
                    description: "123",
                    products: [
                        {
                            id: 1,
                            quantity: 0,
                            unit: "vnt"
                        },
                        {
                            id: 1,
                            quantity: 0,
                            unit: "vnt"
                        },
                    ],
                },
            ],
            modalContent: null
        }
    }

    render() {
        return (
            <div className="container">
                <div>
                    <h1>Visos dietos</h1>
                    <div className="float-md-right"><button type="button" className="btn btn-success">Gauti Dietą!</button></div><br />
                    <hr />
                </div>
                <div className="container">
                    <h5>Kopūstų dieta</h5>
                    Raugintuose produktuose gausu vitaminų ir kitų naudingų medžiagų. Jie turi mažai kalorijų, tačiau ilgam suteikia sotumo jausmą, puikiai tinka vegetarams ir veganams. Siūlome išbandyti trijų dienų trukmės raugintų kopūstų dietą!
                    Preliminarus valgiaraštis (galima rinktis iš siūlomų variantų)<br />
                    Pusryčiai:<br />
                    Avižinė košė, 250 g vaisių ir stiklinė natūralaus jogurto.<br />
                    Riekelė rupios duonos, gabalėlis virtos mėsos raugintų kopūstų marinate (skysčiai).<br />

                    Pietūs:<br />
                    Raugintų kopūstų blyneliai iš tarkuotos bulvės, 2 šaukštų raugintų kopūstų ir kiaušinio.<br />

                    Vakarienė:<br />
                    Rauginti kopūstai, troškinti su žuvimi ir burokėliais.<br />
                    Raugintų kopūstų sriuba su 2 bulvėmis ir žalumynais.<br />
                    <hr />
                </div>
                <div className="container">
                    <h5>Salierų dieta</h5>
                    Ši dieta tinka tiems, kas turi problemų dėl antsvorio pasekmių (išemijos, hipertonijos, hormoninių sutrikimų, tachikardijos, taip pat sergantiems inkstų ir kepenų ligomis). Ji padeda normalizuoti svorį nepablogindama organizmo būklės.
                    Numatytas tik vienas kasdienio meniu variantas. Pagrindinis dietos koziris – salierai. Šis augalas turi daug vitaminų ir organizmui būtinų mineralinių medžiagų (vitamino A, C, K, folio rūgšties, natrio, kalio, vario, geležies, mangano, fosforo, cinko), yra puiki vėžio profilaktikos priemonė. Salierai mažina spaudimą, stiprina kraujagysles, reguliuoja cholesterolio lygį, ramina nervus, lėtina senėjimo procesus, gerina regėjimą, pasižymi antiseptinėmis savybėmis.<br />
                    Dienos meniu<br />
                    Pusryčiams – stiklinė šilto pieno ir 1 šaukštas medaus.<br />
                    Priešpiečiams – gabaliukas ruginės duonos su salierų lapeliais.<br />
                    Pietums – vegetariška sriuba, burokėlių ir salierų salotos, 150 g virtos jautienos.<br />
                    Pavakariams – pomidoras, ruginės duonos džiūvėsėlis.<br />
                    Vakarienei – stiklinė kefyro, gabaliukas juodos duonos, salierų lapeliai.<br />
                    <br />
                    Per dieną būtina išgerti bent 5 stiklines vandens. Meniu galima paįvairinti pakeitus mėsą paukštiena ir neriebia žuvimi, kefyrą – pasukomis ir natūraliu jogurtu, salierų lapus – salierų gumbais ir stiebais.
                    <hr />
                </div>
            </div>
        )
    }
}

export default DietsPage;