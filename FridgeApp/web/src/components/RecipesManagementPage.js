import React from 'react';
import Modal from 'react-modal';

class RecipesManagementPage extends React.Component {
    constructor() {
        super();
        this.state = {
            modalIsOpen: false,
            modal2IsOpen: false,
            recipes: [
                {
                    id: 1,
                    name: "1",
                    description: "123",
                    products: [
                        {
                            id: 1,
                            quantity: 0,
                            unit: "vnt"
                        },
                        {
                            id: 1,
                            quantity: 0,
                            unit: "vnt"
                        },
                    ],
                },
                {
                    id: 2,
                    name: "1",
                    description: "123",
                    products: [
                        {
                            id: 1,
                            quantity: 0,
                            unit: "vnt"
                        },
                        {
                            id: 1,
                            quantity: 0,
                            unit: "vnt"
                        },
                    ],
                },
            ],
            modalContent: null
        }
    }

    handleDeleteRecipe = (index) => {
        console.warn('del');
    }

    openModal = (x) => {
        this.setState(() => ({
            modalIsOpen: true,
            modalContent: x
        }));
    }

    closeModal = () => {
        this.setState(() => ({
            modalIsOpen: false,
        }))
    }

    openModal2 = (x) => {
        this.setState(() => ({
            modalIsOpen2: true,
            modalContent2: x
        }));
    }

    closeModal2 = () => {
        this.setState(() => ({
            modalIsOpen2: false,
        }))
    }

    handleEdit = (id) => {
        this.props.history.push(`/recipes/edit/${id}`)
    }

    handleAdd = () => {
        console.warn('eee');
        this.props.history.push(`/recipes/create`)
    }

    render() {
        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Pavadinimas</th>
                            <th scope="col">Aprašymas</th>
                            <th scope="col">Produktai</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.recipes.map((x, i) => (
                            <tr key={i}>
                                <th scope="row">{i}</th>
                                <td>{x.name}</td>
                                <td>
                                    {x.description.slice(0, 10)}
                                    <button type="button" className="btn btn-sm btn-default ml-2" onClick={() => this.openModal(x)}>...</button>
                                </td>
                                <td>
                                    <button type="button" className="btn btn-sm btn-default ml-2" onClick={() => this.openModal2(x)}>...</button>
                                </td>
                                <td>
                                    <button type="button" className="btn btn-sm btn-success ml-2" onClick={() => this.handleEdit(x.id)}>Redaguoti</button>
                                    <button type="button" className="btn btn-sm btn-danger ml-2" onClick={() => this.handleDeleteRecipe(i)}>Trinti</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button type="button" className="btn btn-primary ml-3" onClick={() => this.handleAdd()}>Pridėti</button>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onRequestClose={this.closeModal}
                    contentLabel="Example Modal"
                >
                    {this.state.modalContent && this.state.modalContent.description}
                    <br />
                    <br />
                    <button className="btn btn-danger" onClick={this.closeModal}>Uždaryti</button>
                </Modal>
                <Modal
                    isOpen={this.state.modalIsOpen2}
                    onRequestClose={this.closeModal2}
                    contentLabel="Example Modal2"
                >
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Pavadinimas</th>
                                <th scope="col">Kiekis</th>
                                <th scope="col">Vienetai</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.modalContent2 && this.state.modalContent2.products.map((x, i) => (
                                <tr key={i}>
                                    <th scope="row">{i}</th>
                                    <td>{x.name}</td>
                                    <td>{x.quantity}</td>
                                    <td>{x.unit}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <br />
                    <br />
                    <button className="btn btn-danger" onClick={this.closeModal2}>Uždaryti</button>
                </Modal>
            </div>
        )
    }
}

export default RecipesManagementPage;