import React from 'react';
import { NavLink } from 'react-router-dom'

class AdminNavigation extends React.Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <a className="navbar-brand" href="#">Išmanusis šaldytuvas</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <NavLink to='/products' className="nav-link" activeClassName='nav-link active'>Produktai</NavLink>
                            </li>
                            {/* <li className="nav-item">
                                <NavLink to='/advertisements' className="nav-link" activeClassName='nav-link active'>Skelbimai</NavLink>
                            </li> */}
                            <li className="nav-item">
                                <NavLink to='/recipes' className="nav-link" activeClassName='nav-link active'>Receptai</NavLink>
                            </li>
                            {/* <li className="nav-item">
                                <NavLink to='/GenerateRecipePage' className="nav-link" activeClassName='nav-link active'>Generuoti Receptą</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to='/ProductsNeedPage' className="nav-link" activeClassName='nav-link active'>Produktų Trūkumas</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to='/StatisticsPage' className="nav-link" activeClassName='nav-link active'>Statistika</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to='/diets' className="nav-link" activeClassName='nav-link active'>Dietos</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to='/discounts' className="nav-link" activeClassName='nav-link active'>Akcijos</NavLink>
                            </li> */}
                        </ul>
                    </div>
                </nav>
                {this.props.children}
            </div>
        );
    }
}

export default AdminNavigation;