import React from 'react';

const products = [
    {
        id: 1,
        name: 'a'
    },
    {
        id: 2,
        name: 'b'
    }
]

class RecipeEditForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "",
            description: "",
            products: [],
            productId: null,
            productQuantity: 0,
            productUnit: ""
        }
    }

    componentDidMount(){
        console.warn(+this.props.match.params.id);
    }

    handleNameChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            name: val
        }));
        console.warn(this.state);
    }

    handleDescriptionChange = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            description: val
        }))
        console.warn(this.state);
    }

    handleChangeProductId = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            productId: +val
        }))
        console.warn(this.state);
    }

    handleChangeProductQuantity = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            productQuantity: +val
        }))
        console.warn(this.state);
    }

    handleChangeProductUnit = (event) => {
        const val = event.target.value;
        this.setState(() => ({
            productUnit: val
        }))
        console.warn(this.state);
    }

    handleAddProduct = () => {
        const { productId, productQuantity, productUnit } = this.state;
        const products = this.state.products;
        products.push({ id: productId, quantity: productQuantity, unit: productUnit });
        this.setState(() => ({
            products
        }))
        console.warn(this.state);
    }

    handleDeleteProduct = (index) => {
        const { products } = this.state;
        products.splice(index, 1);
        this.setState(() => ({
            products
        }));
        console.warn(this.state);
    }

    handleSave = () => {

    }

    render() {
        return (
            <div className="p-5">
                <h2>Redaguoti receptą</h2>
                <form>
                    <div className="form-group">
                        <label>Pavadinimas</label>
                        <input type="text" className="form-control" placeholder="Įveskite pavadinimą" onChange={this.handleNameChange} value={this.state.name} />
                    </div>
                    <div className="form-group">
                        <label>Aprašymas</label>
                        <textarea type="text" className="form-control" placeholder="Įveskite aprašymą" onChange={this.handleDescriptionChange} value={this.state.description} />
                    </div>
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Produktas</th>
                                <th scope="col">Kiekis</th>
                                <th scope="col">Vienetai</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.products.map((x, i) => (
                                <tr key={i}>
                                    <th scope="row">{i}</th>
                                    <td>{products.find(e => e.id === x.id) && products.find(e => e.id === x.id).name}</td>
                                    <td>{x.quantity}</td>
                                    <td>{x.unit}</td>
                                    <td>
                                        <button type="button" className="btn btn-sm btn-danger ml-2" onClick={() => this.handleDeleteProduct(i)}>Trinti</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <div className="row">
                        <div className="col-3">
                            <div className="form-group">
                                <label>Produktas</label>
                                <select className="form-control" onChange={this.handleChangeProductId}>
                                    <option>Pasirinkite produktą</option>
                                    {products.map((x, i) =>
                                        <option key={i} value={x.id}>{x.name}</option>
                                    )}
                                </select>
                            </div>

                        </div>
                        <div className="col-3">
                            <div className="form-group">
                                <label>Kiekis</label>
                                <input type="number" min={0} className="form-control" placeholder="Įveskite Kiekį" onChange={this.handleChangeProductQuantity} value={this.state.productQuantity} />
                            </div>
                        </div>
                        <div className="col-3">
                            <div className="form-group pl-2">
                                <label>Vienetai</label>
                                <input type="text" className="form-control" placeholder="Įveskite vienetus" onChange={this.handleChangeProductUnit} value={this.state.productUnit} />
                            </div>
                        </div>
                        <div className="col-3 d-flex align-self-center pt-3">
                            <button type="button" className="btn btn-success" onClick={this.handleAddProduct}>Pridėti</button>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={this.handleSave}>Saugoti</button>
                </form>
            </div>
        )
    }
}

export default RecipeEditForm;