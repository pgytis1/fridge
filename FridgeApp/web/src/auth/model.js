
// Action types

export const LOGIN = 'auth/LOGIN';
export const LOGIN_SUCESS = 'auth/LOGIN_SUCESS';

// Actions creators

export const actions = {
    login: (userName, password) => ({
        type: LOGIN,
        payload: { userName, password }
    }),
    loginSucces: (id, userName, type) => ({
        type: LOGIN_SUCESS,
        payload: { id, userName, type }
    })
};

// Selectors

export const selectors = {
    getOwn: (state) => state.auth
};