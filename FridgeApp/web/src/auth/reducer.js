import * as m from './model';

const initialState = {
    id: null,
    userName: null,
    type: null
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case m.LOGIN_SUCESS:
            return {
                ...state,
                id: action.payload.id,
                userName: action.payload.userName,
                type: action.payload.type
            };
        default:
            return state;
    }
}